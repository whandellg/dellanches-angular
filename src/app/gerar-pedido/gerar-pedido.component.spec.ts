import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GerarPedidoComponent } from './gerar-pedido.component';

describe('GerarPedidoComponent', () => {
  let component: GerarPedidoComponent;
  let fixture: ComponentFixture<GerarPedidoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GerarPedidoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GerarPedidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
