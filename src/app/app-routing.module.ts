import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CadastrarProdutoComponent } from './cadastrar-produto';
import { GerarPedidoComponent } from './gerar-pedido';
import { HomeComponent } from './home';

const routes: Routes = [
  {path: '', component: HomeComponent },
  {path: 'gerarPedido', component: GerarPedidoComponent },
  {path: 'cadastrarProduto', component: CadastrarProdutoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
