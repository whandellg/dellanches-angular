import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GerarPedidoComponent } from './gerar-pedido/gerar-pedido.component';
import { CadastrarProdutoComponent } from './cadastrar-produto/cadastrar-produto.component';
import { HomeComponent } from './home';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    GerarPedidoComponent,
    CadastrarProdutoComponent,
    HomeComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
